package main

import (
	// "fmt"
	"crypto/subtle"
	"fmt"
	"gitlab.com/clock-8001/clock-8001/v4/clock"
	htmlTemplate "html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"text/template"
	"time"
)

func runHTTP() {
	if options.configFile == "" {
		// No config file specified, can't save the config
		log.Printf("No config specified, http config interface disabled")
		return
	}

	http.HandleFunc("/save", basicAuth(saveHandler))
	http.HandleFunc("/", basicAuth(indexHandler))
	http.HandleFunc("/export", func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Disposition", "attachment;filename=clock.ini")
		http.ServeFile(res, req, options.configFile)
	})
	http.HandleFunc("/import", basicAuth(importHandler))

	log.Printf("HTTP config: listening on %v", options.HTTPPort)
	log.Fatal(http.ListenAndServe(options.HTTPPort, nil))
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := htmlTemplate.New("config.html").Parse(configHTML)
	if err != nil {
		panic(err)
	}

	options.Timezones = tzList

	err = tmpl.Execute(w, options)
	if err != nil {
		panic(err)
	}
}

func importHandler(w http.ResponseWriter, r *http.Request) {

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("import")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	dst, err := os.OpenFile(options.configFile, os.O_RDWR|os.O_CREATE, 0755)
	defer dst.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("Writing new config ini file")
	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	go delayedExit()

	tmpl, err := htmlTemplate.New("confirm.html").Parse(confirmHTML)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(w, nil)
	if err != nil {
		panic(err)
	}
}

// TODO: validation
func saveHandler(w http.ResponseWriter, r *http.Request) {
	var errors string
	var err error

	newOptions.EngineOptions = &clock.EngineOptions{}
	newOptions.EngineOptions.Source1 = &clock.SourceOptions{}
	newOptions.EngineOptions.Source2 = &clock.SourceOptions{}
	newOptions.EngineOptions.Source3 = &clock.SourceOptions{}
	newOptions.EngineOptions.Source4 = &clock.SourceOptions{}

	// Booleans, no validation on them
	newOptions.Debug = r.FormValue("Debug") != ""
	newOptions.DisableHTTP = r.FormValue("DisableHTTP") != ""
	newOptions.EngineOptions.DisableOSC = r.FormValue("DisableOSC") != ""
	newOptions.EngineOptions.DisableFeedback = r.FormValue("DisableFeedback") != ""
	newOptions.EngineOptions.DisableLTC = r.FormValue("DisableLTC") != ""
	newOptions.EngineOptions.LTCSeconds = r.FormValue("LTCSeconds") != ""
	newOptions.EngineOptions.LTCFollow = r.FormValue("LTCFollow") != ""
	newOptions.EngineOptions.Format12h = r.FormValue("Format12h") != ""

	newOptions.EngineOptions.Source1.LTC = r.FormValue("source1-ltc") != ""
	newOptions.EngineOptions.Source1.Timer = r.FormValue("source1-timer") != ""
	newOptions.EngineOptions.Source1.TimerTarget = r.FormValue("source1-timer-target") != ""
	newOptions.EngineOptions.Source1.Tod = r.FormValue("source1-tod") != ""
	newOptions.EngineOptions.Source1.Hidden = r.FormValue("source1-hidden") != ""

	newOptions.EngineOptions.Source2.LTC = r.FormValue("source2-ltc") != ""
	newOptions.EngineOptions.Source2.Timer = r.FormValue("source2-timer") != ""
	newOptions.EngineOptions.Source2.TimerTarget = r.FormValue("source2-timer-target") != ""
	newOptions.EngineOptions.Source2.Tod = r.FormValue("source2-tod") != ""
	newOptions.EngineOptions.Source2.Hidden = r.FormValue("source2-hidden") != ""

	newOptions.EngineOptions.Source3.LTC = r.FormValue("source3-ltc") != ""
	newOptions.EngineOptions.Source3.Timer = r.FormValue("source3-timer") != ""
	newOptions.EngineOptions.Source3.TimerTarget = r.FormValue("source3-timer-target") != ""
	newOptions.EngineOptions.Source3.Tod = r.FormValue("source3-tod") != ""
	newOptions.EngineOptions.Source3.Hidden = r.FormValue("source3-hidden") != ""

	newOptions.EngineOptions.Source4.LTC = r.FormValue("source4-ltc") != ""
	newOptions.EngineOptions.Source4.Timer = r.FormValue("source4-timer") != ""
	newOptions.EngineOptions.Source4.TimerTarget = r.FormValue("source4-timer-target") != ""
	newOptions.EngineOptions.Source4.Tod = r.FormValue("source4-tod") != ""
	newOptions.EngineOptions.Source4.Hidden = r.FormValue("source4-hidden") != ""

	newOptions.EngineOptions.AutoSignals = r.FormValue("auto-signals") != ""
	newOptions.EngineOptions.SignalStart = r.FormValue("signal-start") != ""
	newOptions.SignalFollow = r.FormValue("signal-hw-follow") != ""
	newOptions.SignalToBG = r.FormValue("SignalToBG") != ""

	newOptions.EngineOptions.LimitimerReceive1 = r.FormValue("limitimer-receive-timer1") != ""
	newOptions.EngineOptions.LimitimerReceive2 = r.FormValue("limitimer-receive-timer2") != ""
	newOptions.EngineOptions.LimitimerReceive3 = r.FormValue("limitimer-receive-timer3") != ""
	newOptions.EngineOptions.LimitimerReceive4 = r.FormValue("limitimer-receive-timer4") != ""
	newOptions.EngineOptions.LimitimerReceive5 = r.FormValue("limitimer-receive-timer5") != ""

	newOptions.EngineOptions.LimitimerBroadcast1 = r.FormValue("limitimer-broadcast-timer1") != ""
	newOptions.EngineOptions.LimitimerBroadcast2 = r.FormValue("limitimer-broadcast-timer2") != ""
	newOptions.EngineOptions.LimitimerBroadcast3 = r.FormValue("limitimer-broadcast-timer3") != ""
	newOptions.EngineOptions.LimitimerBroadcast4 = r.FormValue("limitimer-broadcast-timer4") != ""
	newOptions.EngineOptions.LimitimerBroadcast5 = r.FormValue("limitimer-broadcast-timer5") != ""

	newOptions.EngineOptions.PicturallEnabled = r.FormValue("picturall-enabled") != ""
	newOptions.EngineOptions.PicturallLoops = r.FormValue("picturall-loop") != ""
	newOptions.EngineOptions.PicturallStreams = r.FormValue("picturall-streams") != ""
	newOptions.EngineOptions.PicturallMediaName = r.FormValue("picturall-media-name") != ""

	newOptions.EngineOptions.VmixEnabled = r.FormValue("vmix-enabled") != ""
	newOptions.EngineOptions.VmixLoops = r.FormValue("vmix-loops") != ""
	newOptions.EngineOptions.VmixPGMOnly = r.FormValue("vmix-pgm-only") != ""
	newOptions.EngineOptions.VmixPVM = r.FormValue("vmix-show-pvm") != ""
	newOptions.EngineOptions.VmixMediaName = r.FormValue("vmix-media-name") != ""

	// Strings, will not be validated
	newOptions.HTTPUser = r.FormValue("HTTPUser")
	newOptions.HTTPPassword = r.FormValue("HTTPPassword")
	newOptions.EngineOptions.Source1.Text = r.FormValue("source1-text")
	newOptions.EngineOptions.Source2.Text = r.FormValue("source2-text")
	newOptions.EngineOptions.Source3.Text = r.FormValue("source3-text")
	newOptions.EngineOptions.Source4.Text = r.FormValue("source4-text")

	newOptions.EngineOptions.LimitimerSerial = r.FormValue("limitimer-serial")

	newOptions.Matrix = r.FormValue("Matrix")
	newOptions.SerialName = r.FormValue("SerialName")
	errors += validateFile(newOptions.SerialName, "Led ring serial name")

	log.Printf("ASD: %v", r.FormValue("vmix-address"))

	newOptions.EngineOptions.PicturallAddress = r.FormValue("picturall-address")

	newOptions.EngineOptions.VmixAddress = r.FormValue("vmix-address")

	// Limitimer mode
	newOptions.EngineOptions.LimitimerMode = r.FormValue("limitimer-mode")
	if t := newOptions.EngineOptions.LimitimerMode; (t != "off") && (t != "send") && (t != "receive") {
		errors += fmt.Sprintf("<li>Limitimer mode is invalid (%s)</li>", newOptions.EngineOptions.LimitimerMode)
	}

	// UDPTime
	newOptions.EngineOptions.UDPTime = r.FormValue("udp-time")
	if f := newOptions.EngineOptions.UDPTime; (f != "off") && (f != "send") && (f != "receive") {
		errors += fmt.Sprintf("<li>UDP time selection is invalid (%s)</li>", newOptions.EngineOptions.UDPTime)
	}

	// Overtime count mode
	newOptions.EngineOptions.OvertimeCountMode = r.FormValue("overtime-count-mode")
	if f := newOptions.EngineOptions.OvertimeCountMode; (f != "zero") && (f != "blank") && (f != "continue") {
		errors += fmt.Sprintf("<li>Overtime count mode selection is invalid (%s)</li>", newOptions.EngineOptions.OvertimeCountMode)
	}

	// Overtime visibility
	newOptions.EngineOptions.OvertimeVisibility = r.FormValue("overtime-visibility")
	if f := newOptions.EngineOptions.OvertimeVisibility; (f != "blink") && (f != "none") && (f != "background") && (f != "both") {
		errors += fmt.Sprintf("<li>Overtime visibility selection is invalid (%s)</li>", newOptions.EngineOptions.OvertimeVisibility)
	}

	newOptions.Font = r.FormValue("Font")
	errors += validateFile(newOptions.Font, "Font for round clocks")

	// Addresses
	newOptions.EngineOptions.ListenAddr = r.FormValue("ListenAddr")
	errors += validateAddr(newOptions.EngineOptions.ListenAddr, "OSC listen address")
	newOptions.EngineOptions.Connect = r.FormValue("Connect")
	errors += validateAddr(newOptions.EngineOptions.Connect, "OSC feedback address")
	newOptions.HTTPPort = r.FormValue("HTTPPort")
	errors += validateAddr(newOptions.HTTPPort, "HTTP config interface address")

	// Timezones
	newOptions.EngineOptions.Source1.TimeZone = r.FormValue("source1-timezone")
	errors += validateTZ(newOptions.EngineOptions.Source1.TimeZone, "Source 1 timezone")
	newOptions.EngineOptions.Source2.TimeZone = r.FormValue("source2-timezone")
	errors += validateTZ(newOptions.EngineOptions.Source2.TimeZone, "Source 2 timezone")
	newOptions.EngineOptions.Source3.TimeZone = r.FormValue("source3-timezone")
	errors += validateTZ(newOptions.EngineOptions.Source3.TimeZone, "Source 3 timezone")
	newOptions.EngineOptions.Source4.TimeZone = r.FormValue("source4-timezone")
	errors += validateTZ(newOptions.EngineOptions.Source4.TimeZone, "Source 4 timezone")

	// Regexp
	newOptions.EngineOptions.Ignore = r.FormValue("millumin-ignore")
	_, err = regexp.Compile("(?i)" + newOptions.EngineOptions.Ignore)
	if err != nil {
		errors += fmt.Sprintf("<li>Millumin layer ignore regexp: %v</li>", err)
	}

	// Integers
	newOptions.EngineOptions.Flash, err = strconv.Atoi(r.FormValue("Flash"))
	errors += validateNumber(err, "Flash time")
	newOptions.EngineOptions.Timeout, err = strconv.Atoi(r.FormValue("Timeout"))
	errors += validateNumber(err, "Tally message timeout")
	newOptions.EngineOptions.Source1.Counter, err = strconv.Atoi(r.FormValue("source1-counter"))
	errors += validateNumber(err, "Source 1 timer")
	errors += validateTimer(newOptions.EngineOptions.Source1.Counter, "Source 1 timer")
	newOptions.EngineOptions.Source2.Counter, err = strconv.Atoi(r.FormValue("source2-counter"))
	errors += validateNumber(err, "Source 2 timer")
	errors += validateTimer(newOptions.EngineOptions.Source2.Counter, "Source 2 timer")
	newOptions.EngineOptions.Source3.Counter, err = strconv.Atoi(r.FormValue("source3-counter"))
	errors += validateNumber(err, "Source 3 timer")
	errors += validateTimer(newOptions.EngineOptions.Source3.Counter, "Source 3 timer")
	newOptions.EngineOptions.Source4.Counter, err = strconv.Atoi(r.FormValue("source4-counter"))
	errors += validateNumber(err, "Source 4 timer")
	errors += validateTimer(newOptions.EngineOptions.Source4.Counter, "Source 4 timer")
	newOptions.EngineOptions.Mitti, err = strconv.Atoi(r.FormValue("mitti"))
	errors += validateNumber(err, "Mitti destination timer")
	errors += validateTimer(newOptions.EngineOptions.Mitti, "Mitti destination timer")
	newOptions.EngineOptions.Millumin, err = strconv.Atoi(r.FormValue("millumin"))
	errors += validateNumber(err, "Millumin destination timer")
	errors += validateTimer(newOptions.EngineOptions.Millumin, "Millumin destination timer")
	newOptions.EngineOptions.ShowInfo, err = strconv.Atoi(r.FormValue("ShowInfo"))
	errors += validateNumber(err, "Time to show clock info on startup")

	newOptions.EngineOptions.UDPTimer1, err = strconv.Atoi(r.FormValue("udp-timer-1"))
	errors += validateNumber(err, "UDP Timer 1")

	newOptions.EngineOptions.UDPTimer2, err = strconv.Atoi(r.FormValue("udp-timer-2"))
	errors += validateNumber(err, "UDP Timer 2")

	newOptions.EngineOptions.SignalThresholdWarning, err = strconv.Atoi(r.FormValue("signal-threshold-warning"))
	errors += validateNumber(err, "Warning signal threshold")

	newOptions.EngineOptions.SignalThresholdEnd, err = strconv.Atoi(r.FormValue("signal-threshold-end"))
	errors += validateNumber(err, "End signal threshold")

	newOptions.EngineOptions.SignalHardware, err = strconv.Atoi(r.FormValue("signal-hw-group"))
	errors += validateNumber(err, "Signal hardware group")

	newOptions.SerialBaud, err = strconv.Atoi(r.FormValue("SerialBaud"))
	errors += validateNumber(err, "Serial baud rate")

	newOptions.EngineOptions.PicturallPort, err = strconv.Atoi(r.FormValue("picturall-port"))
	errors += validateNumber(err, "Picturall port")
	newOptions.EngineOptions.PicturallTimer, err = strconv.Atoi(r.FormValue("picturall-timer"))
	errors += validateNumber(err, "Picturall timer")
	newOptions.EngineOptions.PicturallTimeout, err = strconv.Atoi(r.FormValue("picturall-timeout"))
	errors += validateNumber(err, "Picturall timeout")

	newOptions.EngineOptions.VmixPort, err = strconv.Atoi(r.FormValue("vmix-port"))
	errors += validateNumber(err, "vMix port")
	newOptions.EngineOptions.VmixTimer, err = strconv.Atoi(r.FormValue("vmix-timer"))
	errors += validateNumber(err, "vMix timer")
	newOptions.EngineOptions.VmixInterval, err = strconv.Atoi(r.FormValue("vmix-interval"))
	errors += validateNumber(err, "vMix interval")
	newOptions.EngineOptions.VmixTimeout, err = strconv.Atoi(r.FormValue("vmix-timeout"))
	errors += validateNumber(err, "vMix timeout")

	// Picturall ignore layers
	picturallIgnoreLayers := r.FormValue("picturall-ignore-layers")
	if ok, err := regexp.MatchString(`^(?:\d+,?)+$`, picturallIgnoreLayers); len(picturallIgnoreLayers) == 0 || (ok && err == nil) {
		newOptions.EngineOptions.PicturallIgnoreLayers = picturallIgnoreLayers
	} else {
		errors += "<li>Picturall layer ignore list is not valid</li>"
	}

	// vMix ignore overlays
	vmixIgnoreOverlays := r.FormValue("vmix-ignore-overlays")
	if ok, err := regexp.MatchString(`^(?:\d+,?)+$`, vmixIgnoreOverlays); len(vmixIgnoreOverlays) == 0 || (ok && err == nil) {
		newOptions.EngineOptions.VmixIgnoreOverlays = vmixIgnoreOverlays
	} else {
		errors += "<li>vMix overlay ignore list is not valid</li>"
	}

	// Colors
	newOptions.TextColor = r.FormValue("TextColor")
	errors += validateColor(newOptions.TextColor, "Clock text color")
	newOptions.BackgroundColor = r.FormValue("BackgroundColor")
	errors += validateColor(newOptions.BackgroundColor, "Clock background color")
	newOptions.CountdownColor = r.FormValue("CountdownColor")
	errors += validateColor(newOptions.CountdownColor, "Aux countdown color")

	newOptions.EngineOptions.SignalColorStart = r.FormValue("signal-color-start")
	errors += validateColor(newOptions.EngineOptions.SignalColorStart, "Signal color: start")
	newOptions.EngineOptions.SignalColorWarning = r.FormValue("signal-color-warning")
	errors += validateColor(newOptions.EngineOptions.SignalColorStart, "Signal color: warning")
	newOptions.EngineOptions.SignalColorEnd = r.FormValue("signal-color-end")
	errors += validateColor(newOptions.EngineOptions.SignalColorStart, "Signal color: end")

	newOptions.EngineOptions.Source1.OvertimeColor = r.FormValue("source1-overtime-color")
	errors += validateColor(newOptions.EngineOptions.Source1.OvertimeColor, "Source1 overtime color")
	newOptions.EngineOptions.Source2.OvertimeColor = r.FormValue("source2-overtime-color")
	errors += validateColor(newOptions.EngineOptions.Source2.OvertimeColor, "Source2 overtime color")
	newOptions.EngineOptions.Source3.OvertimeColor = r.FormValue("source3-overtime-color")
	errors += validateColor(newOptions.EngineOptions.Source3.OvertimeColor, "Source3 overtime color")
	newOptions.EngineOptions.Source4.OvertimeColor = r.FormValue("source4-overtime-color")
	errors += validateColor(newOptions.EngineOptions.Source4.OvertimeColor, "Source4 overtime color")

	newOptions.EngineOptions.PicturallMediaColor = r.FormValue("picturall-media-color")
	errors += validateColor(newOptions.EngineOptions.PicturallMediaColor, "Picturall media text color")
	newOptions.EngineOptions.PicturallMediaBG = r.FormValue("picturall-media-bg")
	errors += validateColor(newOptions.EngineOptions.PicturallMediaBG, "Picturall media background color")

	newOptions.EngineOptions.VmixMediaColor = r.FormValue("vmix-media-color")
	errors += validateColor(newOptions.EngineOptions.VmixMediaColor, "vMix media text color")
	newOptions.EngineOptions.VmixMediaBG = r.FormValue("vmix-media-bg")
	errors += validateColor(newOptions.EngineOptions.VmixMediaBG, "vMix media background color")

	if errors != "" {
		tmpl, err := htmlTemplate.New("config.html").Parse(configHTML)
		if err != nil {
			panic(err)
		}
		newOptions.Errors = htmlTemplate.HTML(fmt.Sprintf("<ul>%s</ul>", errors))
		err = tmpl.Execute(w, newOptions)
		if err != nil {
			panic(err)
		}
	} else {
		log.Printf("Writing new config ini file")
		newOptions.writeConfig(options.configFile)

		go delayedExit()

		// Render success page

		tmpl, err := htmlTemplate.New("confirm.html").Parse(confirmHTML)
		if err != nil {
			panic(err)
		}
		err = tmpl.Execute(w, nil)
		if err != nil {
			panic(err)
		}
	}
}

// Reboot the pi after a short delay
// delay needs to be shorter than the
// delayedExit()...
func delayedReboot() {
	time.Sleep(time.Millisecond * 500)
	cmd := exec.Command("reboot")
	cmd.Env = os.Environ()
	if err := cmd.Run(); err != nil {
		panic(err)
	}
}

func delayedExit() {
	time.Sleep(time.Second)
	confChan <- true
	// os.Exit(0)
}

func (options *clockOptions) writeConfig(path string) {
	tmpl, err := template.New("config.ini").Parse(configTemplate)
	if err != nil {
		panic(err)
	}
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(f, options)
	f.Sync()
	f.Close()
}

func (options *clockOptions) createHTML() {
	tmpl, err := htmlTemplate.New("config.html").Parse(configHTML)
	if err != nil {
		panic(err)
	}

	err = tmpl.Execute(os.Stdout, options)
	if err != nil {
		panic(err)
	}
}

func basicAuth(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()

		if !ok || subtle.ConstantTimeCompare([]byte(options.HTTPUser), []byte(user)) != 1 || subtle.ConstantTimeCompare([]byte(options.HTTPPassword), []byte(pass)) != 1 {
			w.Header().Set("WWW-Authenticate", `Basic realm="Clock-8001 config"`)
			w.WriteHeader(401)
			w.Write([]byte("Unauthorised.\n"))
			return
		}

		handler(w, r)
	}
}

func validateFile(filename, title string) (msg string) {
	if !fileExists(filename) {
		msg = fmt.Sprintf("<li>%s: file does not exist (%s)</li>", title, filename)
	}
	return
}

func validateNumber(err error, title string) (msg string) {
	if err != nil {
		msg = fmt.Sprintf("<li>%s: error parsing number</li>", title)
	}
	return
}

func validateTimer(timer int, title string) (msg string) {
	if timer < 0 || timer > 9 {
		msg = fmt.Sprintf("<li>%s: timer number not in range 0-9 (%d)</li>", title, timer)
	}
	return
}

func validateColor(color string, title string) (msg string) {
	match, err := regexp.MatchString(`^#([0-9a-fA-F]{3}){1,2}$`, color)

	if !match {
		msg = fmt.Sprintf("<li>%s: incorrect format for a color (%s)</li>", title, color)
	} else if err != nil {
		msg = fmt.Sprintf("<li>%s: %v</li>", title, err)
	}
	return
}

func validateAddr(addr, title string) (msg string) {
	match, err := regexp.MatchString(`^.*:\d*$`, addr)

	if !match {
		msg = fmt.Sprintf("<li>%s: address not formatted correctly (%s)</li>", title, addr)
	} else if err != nil {
		msg = fmt.Sprintf("<li>%s: %v</li>", title, err)
	}
	return
}

func validateTZ(zone, title string) (msg string) {
	_, err := time.LoadLocation(zone)
	if err != nil {
		msg = fmt.Sprintf("<li>%s: %v</li>", title, err)
	}
	return
}

// fileExists checks if a file exists and is not a directory
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
